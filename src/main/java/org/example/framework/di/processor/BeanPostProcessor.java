package org.example.framework.di.processor;

import java.util.List;

@FunctionalInterface
public interface BeanPostProcessor {
    Object process(Object bean, Class<?> originalClass, List<Object> args, List<Class<?>> argTypes);
}
