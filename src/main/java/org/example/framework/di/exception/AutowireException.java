package org.example.framework.di.exception;

public class AutowireException extends RuntimeException {
    public AutowireException() {
    }

    public AutowireException(String message) {
        super(message);
    }

    public AutowireException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutowireException(Throwable cause) {
        super(cause);
    }

    public AutowireException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
