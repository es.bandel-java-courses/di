package org.example.framework.di.exception;

public class NameAlreadyRegisteredException extends RuntimeException {
  public NameAlreadyRegisteredException() {
  }

  public NameAlreadyRegisteredException(String message) {
    super(message);
  }

  public NameAlreadyRegisteredException(String message, Throwable cause) {
    super(message, cause);
  }

  public NameAlreadyRegisteredException(Throwable cause) {
    super(cause);
  }

  public NameAlreadyRegisteredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
