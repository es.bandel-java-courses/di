package org.example.framework.di.exception;

public class RegisterTwiceException extends RuntimeException {

    public RegisterTwiceException() {
    }

    public RegisterTwiceException(String message) {
        super(message);
    }

    public RegisterTwiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegisterTwiceException(Throwable cause) {
        super(cause);
    }

    public RegisterTwiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
