package org.example.framework.di.exception;

public class UnmetDependenciesException extends RuntimeException {
  public UnmetDependenciesException() {
  }

  public UnmetDependenciesException(String message) {
    super(message);
  }

  public UnmetDependenciesException(String message, Throwable cause) {
    super(message, cause);
  }

  public UnmetDependenciesException(Throwable cause) {
    super(cause);
  }

  public UnmetDependenciesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
